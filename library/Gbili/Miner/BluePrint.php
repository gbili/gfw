<?php
namespace Gbili\Miner;

use Gbili\Out\Out,
    Gbili\Url\Authority\Host,
    Gbili\Db\Registry                                                as DbRegistry,
    Gbili\Miner\BluePrint\Db\DbInterface,
    Gbili\Miner\BluePrint\Action\CMLoader,
    Gbili\Miner\BluePrint\Action\AbstractAction,
    Gbili\Miner\BluePrint\Action\GetContents,
    Gbili\Miner\BluePrint\Action\Extract,
    Gbili\Miner\BluePrint\Action\Extract\Method\Wrapper       as ExtractMethodWrapper,
    Gbili\Miner\BluePrint\Action\GetContents\Callback\Wrapper as GetContentsCallbackWrapper;

/**
 * The BluePrint takes a host as constructor parameter and with that it will
 * try to reconstruct a previously saved (with \\Gbili\\Miner\\\BluePrint_Savable)
 * action tree. For that it queries the storage (for example the database) using the
 * DbRegistry which must return an instanceof \\Gbili\\Miner\\\BluePrint_Db_Interface.
 * @see DbRegistry
 * The returned instance contains all the information the bluePrint needs to create
 * an action tree. The action tree may contain two types of actions :
 * 1) Extract
 * 		Extracts bits (parts) of data from the plain text other actions pass to it.
 * 		It takes input either from Extract or GetContents actions.
 * 2) GetContents
 * 		Gets the text from the web, given a string url.
 * 		It takes its input from root data or Extract actions.
 * The bluePrint constructs this tree
 * 
 * 
 * 
 * @author gui
 *
 */
class BluePrint
{
	/**
	 * The type of action
	 * Extract
	 * 
	 * @var integer
	 */
	const ACTION_TYPE_EXTRACT = 12;
	
	/**
	 * The type of action
	 * GetContents
	 * 
	 * @var integer
	 */
	const ACTION_TYPE_GETCONTENTS = 13;
	
	/**
	 * Contains one action \\Gbili\\Miner\\\BluePrint_Action_Abstractfrom
	 * which all other actions are accessible
	 * 
	 * @var unknown_type
	 */
	private $lastAction;
	
	/**
	 * this is a flat representation of the
	 * actions tree
	 * So it eases access to actions
	 * 
	 * @var unknown_type
	 */
	private $actionStack = array();
	
	/**
	 * Miner_Engine::run(), will generate instances
	 * where the actions results will be inserted.
	 * This tells at which action (from id) \\Gbili\\Miner\\\BluePrint
	 * has to call setAsNewInstanceGeneratingPoint()
	 * so Miner_Engine knows that when it reaches that
	 * action it has to generate a new instance.
	 * 
	 * @var unknown_type
	 */
	private $newInstanceGeneratingPointActionId = null;
	
	/**
	 * Every time an action has been successfully added
	 * to the action chain, then this variable takes
	 * the value of its parent it
	 * So it can be remebered for next action, and help
	 * determining, if the next action is brother (has same parent)
	 * or is child
	 * 
	 * @var unknown_type
	 */
	private $lastParentId = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $currentAction = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $methodClassInstance = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $callbackClassInstance = null;
	
	/**
	 * 
	 * @var Host
	 */
	private $host;
	
	/**
	 * Will generate the actions chain from the action set
	 * from Db passed as argument
	 * This is called from \\Gbili\\Miner\\\BluePrint::factory()
	 * 
	 * @param Host $host
	 * @return unknown_type
	 */
	public function __construct(Host $host)
	{
		$this->host = $host;
		//get the bluePrint info from db and set it in instance
		$this->setInfo();
		/*
		 * after calling setInfo() $callbackClassInstance and 
		 * $methodClassInstance must be set if available
		 * and there is new instance generating point action id
		 * fetch the Db to get the action set
		 */
		$recordset = DbRegistry::getInstance('\\Gbili\\Miner\\\BluePrint')->getActionSet($this->host);
		//ensure there are rows
		if (false === $recordset) {
			throw new Exception(
			        'There is no action set for this url : ' 
			        . $this->host->getHost()->toString()
			        );
		}
		
		//create an action per recordset row
		foreach ($recordset as $row) {
			//determine the action type and populate $this->currentAction
			if ((integer) $row['type'] === self::ACTION_TYPE_EXTRACT) {
				$this->initActionExtract($row);
			} else if ((integer) $row['type'] === self::ACTION_TYPE_GETCONTENTS) {
				$this->initActionGetContents($row);
			} else {
				throw new Exception(
				        'Unsupported action type given : ' 
				        . print_r($row, true)
				        );
			}
			$this->currentAction->setTitle($row['title']);
			$this->currentAction->setId($row['actionId']);
			//determine whether the action is the videoEntityStartingPoint
			if ($this->currentAction->getId() === $this->newInstanceGeneratingPointActionId) {
				Out::l2("setting new instance generating point \n");
				$this->currentAction->setAsNewInstanceGeneratingPoint();
			} else {
				Out::l2("is not new instance generating point \n");
			}
			//add to stack
			$this->actionStack[(integer) $row['actionId']] = $this->currentAction;
			//add the action to chain and set the input group (if needed)
			$this->chain((integer) $row['parentId'], $row['inputGroupNum']);
			//pass a reference of the blue print to every action
			$this->currentAction->setBluePrint($this);
		}
		//ensure the instance starting point action is present
		if (null === $this->newInstanceGeneratingPointActionId) {
			throw new Exception(
			        'The actionId specified as video entity starting point is' 
			        . ' not present'
			        );
		}
		//set the injection hook data
		if (false !== ($injectData = DbRegistry::getInstance('\\Gbili\\Miner\\\BluePrint')
		                                                ->getInjectionData($row['actionId']))) {
			$this->currentAction()
			->setOtherInputActionInfo($injectData['injectingActionId'],
			         (((integer) $injectData['inputGroup'] === 0)? null : $injectData['inputGroup']));
		}
		
		//now pass the root pointer to the bluePrint
		$this->lastAction = $this->currentAction->getRoot();
		//print_r($this->lastAction->getChild());
	}
	
	/**
	 * Set the method and callback instances
	 * and the new instance generating point action id
	 * 
	 * @return unknown_type
	 */
	private function setInfo()
	{
		$dbRegObj = DbRegistry::getInstance('\\Gbili\\Miner\\\BluePrint');
		if (!($dbRegObj instanceof DbInterface)) {
			throw new Exception('The DbRegistry must return an instanceof \\Gbili\\Miner\\\BluePrint\\Db\\DbInterface');
		}
		$bPRecordset = $dbRegObj->getBluePrintInfo($this->host);
		if (false === $bPRecordset) {
			throw new Exception('The bluePrint does not exist');
		}
		
		foreach ($bPRecordset as $record) {
			Out::l1($record);
			if (!isset($record['path'])) { //if path is present all other should be too
				break;
			}
			$this->loadCMClass($record['path'], (integer) $record['pathType'], (integer) $record['classType']);
		}

		//set the new instance generating point action id
		if (0 === (integer) $record['newInstanceGeneratingPointActionId']) {
			throw new Exception('there must be a newInstanceGeneratingPointActionId');
		}
		$this->newInstanceGeneratingPointActionId = (integer) $record['newInstanceGeneratingPointActionId'];
		//try to get the instances
		//initialize as false
	}
	
	/**
	 * Instantiate callback and method classes using CM loader
	 * 
	 * @param unknown_type $path
	 * @param unknown_type $pathType
	 * @param unknown_type $classType
	 * @return unknown_type
	 */
	private function loadCMClass($path, $pathType, $classType)
	{
		//hack for base path to try to load both class types
		if (CMLoader::PATH_TYPE_BASE === $pathType) {
			$classTypes = array(CMLoader::CLASS_TYPE_CALLBACK,
							    CMLoader::CLASS_TYPE_METHOD);
		} else {
			$classTypes = array($classType);
		}

		foreach ($classTypes as $classType) {
			$className = CMLoader::loadCMClass($path, $this->host, $pathType, $classType);
			if (is_string($className)) {
				if (CMLoader::CLASS_TYPE_CALLBACK === $classType) {
					$this->callbackClassInstance = new $className();
				} else {
					$this->methodClassInstance = new $className();
				}
			} else {
				Out::l2("class not loaded : " . print_r(CMLoader::getErrors(), true));
			}
		}
	}
	
	/**
	 * This function creates an action instance that it makes
	 * available to the constructor by setting $this->currentAction
	 * 
	 * @param array $info
	 * @return unknown_type
	 */
	private function initActionGetContents(array $info)
	{
		Out::l2("initializing action getContents\n");
		//if it is root action
		if ($info['actionId'] === $info['parentId']) {
			//then the input data is contained in data
			//if the constructor has a parameter then it is considered as the input data and the root action
			$this->currentAction = new GetContents($info['data']);
		} else {
			$this->currentAction = new GetContents();
		}
		$this->currentAction->setAsOptional($info['isOpt']);
		//see if it uses callback
		$callbackInfo = DbRegistry::getInstance($this)->getActionCallbackMethodName($info['actionId']);
		if (false !== $callbackInfo) {
			if (null === $this->callbackClassInstance) {
				throw new Exception('the current action extract wants to use method hook, but the bluePrint did not manage to instantiate the method class');
			}
			$row = current($callbackInfo);
			Out::l1($row['methodName']);
			//if the callbacInstance is null it will throw an exception because of param type hint
			$cW = new GetContentsCallbackWrapper($this->callbackClassInstance, $row['methodName']);
			
			//not all callbacks have a mapping (ex: a root get contents that uses itself as input)
			$callbackMapping = DbRegistry::getInstance($this)->getActionCallbackParamsToGroupMapping($info['actionId']);
			Out::l2("retrieving action callback params to group mapping : \n" . print_r($callbackMapping, true));
			$callbackParamGroupMapping = array();//set blank mapping
			if (false !== $callbackMapping) {
				//reshape array
				foreach ($callbackMapping as $row) {
					$callbackParamGroupMapping[$row['paramNum']] = $row['regexGroup'];
				}
			} else {
				//default group mapping
				$callbackParamGroupMapping = array(1);
			}
			$cW->setParamToGroupMapping($callbackParamGroupMapping);
			$this->currentAction->setCallbackWrapper($cW);
		}
	}
	
	/**
	 * 
	 * @param array $info
	 * @return void
	 */
	private function initActionExtract(array $info)
	{
		Out::l2("initializing action extract\n");
		//create the instance
		$this->currentAction = new Extract($info['data'], (1 === (integer) $info['useMatchAll']));
		Out::l2("initActionExtract Id : {$info['actionId']}, regexData : " . print_r($info['data'], true));
		//query Db to get the group mapping (final results mapping)
		$groupMapping = DbRegistry::getInstance($this)->getActionGroupToEntityMapping($info['actionId']);
		//ensure there are rows
		if (false !== $groupMapping) {
			//set the group mapping even if empty
			$this->currentAction->setGroupMapping($groupMapping);
		}
		$this->currentAction->setAsOptional($info['isOpt']);
		$interceptMap = DbRegistry::getInstance($this)->getActionGroupToMethodNameAndInterceptType($info['actionId']);
		if (false !== $interceptMap) {
			Out::l2("uses intercept\n" . print_r($interceptMap, true));
			if (null === $this->methodClassInstance) {
				throw new Exception('the current action extract wants to use method hook, but the bluePrint did not manage to instantiate the method class');
			}
			$mW = new ExtractMethodWrapper($this->methodClassInstance, $interceptMap);
			Out::l1($this->methodClassInstance);
			$this->currentAction->setMethodWrapper($mW);
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getCallbackInstance()
	{
		if (null === $this->callbackClassInstance) {
			throw new Exception('There is no callback instance set in this blue print');
		}
		return $this->callbackClassInstance;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getMethodInstance()
	{
		if (null === $this->methodClassInstance) {
			throw new Exception('There is no callback instance set in this blue print');
		}
		return $this->methodClassInstance;
	}
	
	/**
	 * Chains the action to the right parent
	 * and sets from which parent group the 
	 * action must take its input data (if it
	 * is an instance of extract)
	 * 
	 * @param $action
	 * @param $currentParentId
	 * @param $inputDataGroup
	 * @return unknown_type
	 */
	private function chain($currentParentId, $inputDataGroup)
	{
		//first time entering chain should be root
		if ($this->currentAction->getId() === $currentParentId) {
			$this->currentAction->setAsRoot();
		} else {
			//find the right parent and add child
			$this->findParent($currentParentId)->addChild($this->currentAction);
		}
		//Point the chain to the last action
		$this->lastAction = $this->currentAction;
		//also set the group for input
		if ($this->lastAction->getParent() instanceof Extract) {
			$this->lastAction->setInputDataGroup($inputDataGroup);
		}
	}
	
	/**
	 * 
	 * @param unknown_type $parentId
	 * @return unknown_type
	 */
	private function findParent($parentId)
	{
		//get the youngest action
		$action = $this->lastAction;
		while ($action->getId() !== $parentId) {
			if ($action->isRoot()) {
				throw new Exception("Did not find parent in stack while rolling back");
			}
			$action = $action->getParent();
		}
		return $action;
	}
	
	/**
	 * Proxy
	 * 
	 * @return unknown_type
	 */
	public function getRoot()
	{
		if ($this->lastAction === null || 
		  !($this->lastAction instanceof AbstractAction)) {
		  	throw new Exception('There are no actions in blueprint $this->lastAction : ' . print_r($this->lastAction, true));
		}
		return $this->lastAction->getRoot();
	}
	
	/**
	 * Returns the action with the id
	 * 
	 * @return unknown_type
	 */
	public function getAction($id)
	{
		$id = (integer) $id;
		if (!isset($this->actionStack[$id])) {
			throw new Exception("There is no action with id : $id in bluePrint");
		}
		//Out::l1($this->actionStack[$id]);
		return $this->actionStack[$id];
	}
	
	/**
	 * 
	 * @param unknown_type $actionId
	 * @param unknown_type $input
	 * @return unknown_type
	 */
	public static function updateActionInputData($actionId, $input)
	{
		DbRegistry::getInstance('\\Gbili\\Miner\\\BluePrint\\Action\\Savable')->updateActionInputData($actionId, $input);
	}
}