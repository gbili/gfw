<?php
namespace Gbili\Miner;

use Gbili\Url\Authority\Host,
    Gbili\Out\Out,
    Gbili\Slug\Slug,
    Gbili\Db\ActiveRecord\ActiveRecordInterface,
    Gbili\Db\Registry          as DbRegistry,
    Gbili\Miner\Lexer\Registry as LexerRegistry,
    Gbili\Miner\BluePrint,
    Gbili\Miner\BluePrint\Action\AbstractAction,
    Gbili\Miner\BluePrint\Action\GetContents,
    Gbili\Miner\BluePrint\Action\Extract;


/**
 * @todo update graphic, current deprecated                                               
 *                                    __________________________ 
 *                                    @ BluePrint @
 *                                    @_________________________@
 *                                      ^           |
 *       ____________________________   |   ________|_________________________
 *       %                          %   |   @                                 @
 *       % Db_Req 				%  (1)  @  AbstractAction@ (chain)
 *       %__________________________%   |   @_________________________________@
 *                ^                     |           |
 *                |                     |          (2)
 *              save                    |           |
 *                |                    _|___________v__________________
 *                |______(8)__loop____@                                @
 *                                    @                                @
 *       	                          @          Miner_Engine       @                   wwww content (not understandable by application)
 *      __________________            @                                @
 *      @ currentInstance @           @                                @
 *      @________________ @           @                                @
 *             ^                      @_________________loop___________@
 *             |                          |              ^    ^
 *         finalResults             @ currentInstance @  |    |           
 *        	  (7)                     finalResults       |    |           		
 *        _____|__________________        |              | finalResults   
 *        @                       @      (6)            (3)   |          
 *        @ Miner_Engine_Lexer @<______|              |   (5)?
 *        @_______________________@         _____________v____|_____________
 *       								   @ AbstractAction@<---(4)->Retrieve content
 * 								           @________________________________@
 * 
 * The Miner_Engine is in charge of instantiating a (1) BluePrint
 * then it will retrieve it's (2) AbstractActioninstances
 * chain and from there it will start the dumping process. (3)
 * Every time a AbstractActionhas final results (5)
 * the Miner_Engine will get an instance of a Miner_Engine_Lexer_Abstract subclass (6)
 * from the LexerRegistry, and pass the AbstractAction
 * final results along with the current instance being populated 
 * to Miner_Engine_Lexer_Abstract->populateInstance(:instance, :finalResults)
 * 
 * From there the Miner_Engine_Lexer_Abstract subclass should know how to handle the final results. (7)
 * @see Miner_Engine_Lexer_Abstract (roughly each final result is associated to a term, and each
 * term triggers an instance setter method, the lexer can be thought as a dictionary where each
 * word triggers a method)
 * 
 * Once the instance is considered full (there are no more actions for the current instance)
 * then a new instance is created from Miner_Engine::$instancesClassName
 * 
 * Once Miner_Engine::$numOfInstancesInStackBeforeSave count is reached, then all the
 * instances in stack Miner_Engine::instancesArray are saved by calling the method save()
 * on each one, that is why they must implement ActiveRecordInterface (8)
 * 
 * 
 * 
 * To instantiate Miner_Engine (from now on : Engine), you must provide a host as a string that
 * it will use to create an instance of Host (cleaner code) or directly as an 
 * instance (you will write more code for nothing). With that host the engine tries to create a 
 * BluePrint instance, whose contents you will have previously created and saved
 * with the help of BluePrint_Savable.
 * @see BluePrint_Savable to learn how create Engine useable bluePrints
 * 
 * 
 * 
 * 
 * 
 * 
 * @author gui
 *
 */
class Engine
{	
	const REACHED_LEAF = 10;
	const END_OF_BLUEPRINT_EXECUTION = 11;
	const ACTION_EXECUTION_SUCCEED = 12;
	const NEXT_ACTION_IS_READY_TO_EXECUTE = 13;
	const SKIP_LEAF = 14;
	const AWAITINCHILD_RESULT = 15;
	const CHANGE_FLOW = 17;
	
	/**
	 * Try catch exceptions when the instance throws one
	 * 
	 * @var unknown_type
	 */
	public static $skipToNextInstanceWhenExceptionOccurs = true;
	
	/**
	 * When $exceptionSuccessDifferenceCount reaches this
	 * number, the try catch block exits the script this means:
	 * Allow $maxExceptionsCatchPerSuccessNumber of exceptions
	 * + number of successes, before exiting the script.
	 * 
	 * @var unknown_type
	 */
	public static $maxExceptionsCatchPerSuccessNumber = 5;
	
	/**
	 * Exceptions add 1 to count
	 * Successes rest 1 to count
	 * If exceptionSuccessFailDifferenceCount >= self::$maxExceptionCatchPerSuccessNumber
	 * exit the script
	 * 
	 * @var unknown_type
	 */
	private $numberOfRemainingFailsAllowed = 5;
	
	/**
	 * Miner_Engine_Lexer_Abstract instance
	 * will populate instances of this type
	 * The class must implement ActiveRecordInterface
	 * 
	 * @var string
	 */
	public static $instancesClassName = 'Vid_Savable';
	
	/**
	 * Contains the class instances that are created from the results
	 * 
	 * @var array
	 */
	private $instancesArray;
	
	/**
	 * Is the instance that will be filled by the lexer
	 * 
	 * @var determined in self::$instancesClassName
	 */
	private $currentInstance;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $bluePrint;
	
	/**
	 * When actions spit content before the newInstanceGeneratingPoint
	 * has been executed, the currentInstance does not exist. Thereafter
	 * the content must be spit to the lexer bucket, so it can be injected
	 * once the instance is available.
	 * Once the nIGPAction has been executed, the currentAction is allways
	 * available, so the only way to set this right is to set it before
	 * nIGPAction gets executed the first time
	 * 
	 * @var unknown_type
	 */
	private $actionsNeedingToSpitToLexerBucket = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $numOfInstancesInStackCount;
	
	/**
	 * SAS = skip and switch
	 * @var unknown_type
	 */
	private static $numOfInstancesBeforeSASToBrother = 0;
	
	/**
	 * Deepnes control action id
	 * when engine executes this action it will
	 * check for numOfInstancesBeforeSasCount, and
	 * if it is = 0 then it will skip exec and switch to brother
	 * @var unknown_type
	 */
	private static $sASActionId = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $numOfInstancesBeforeSASCount = 0;
	
	/**
	 * @var unknown_type
	 */
	private $currentAction = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $nIGPLastInput = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $errorTriggerActionId = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $actionIdAwaitingActionIdResult = array();
	
	/**
	 * Action id => result
	 * @var unknown_type
	 */
	private $actionResultForAwaitingActions = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	private static $numOfInstancesInStackBeforeSave = 1;
	
	/**
	 * Character set of the site being dumped
	 * 
	 * @var unknown_type
	 */
	private static $inputCharSet;
	
	/**
	 *
	 * @var Miner_Engine_Lexer_Abstract
	 */
	private $lexerInstance = null;
	
	/**
	 * Instanciate the blue print
	 * 
	 * @param Url $url
	 * @return unknown_type
	 */
	public function __construct($host)
	{
		if (is_string($host)) {
			$host = new Host($host);
		} else if ($host instanceof Host) {
			throw new Exception('constructor first param must be a valid url string or a Host instance');
		}
		$bluePrint = new BluePrint($host);
		if (!($bluePrint instanceof BluePrint)) {
			throw new Exception('Cannot create BluePrint for given authority : ' . print_r($host->toString(), true));
		}
		$this->bluePrint = $bluePrint;
		
		$this->numOfInstancesInStackCount = 0;
	}
	
	/**
	 * 
	 * @param unknown_type $secs
	 * @return unknown_type
	 */
	public static function setExecTimeLimit($secs)
	{
		set_time_limit((integer) $secs);
	}
	
	/**
	 * 
	 * @param unknown_type $secs
	 * @return unknown_type
	 */
	public static function setHttpRequestTimeInterval($minSecs, $maxSecs = null)
	{
		GetContents::setSecondsDelayBetweenRequests($minSecs, $maxSecs);
	}
	
	/**
	 * This will make the thread skip and switch
	 * to brother execution even if more results can
	 * be generated, when the number of instances
	 * specified in param have been created
	 * 
	 * It allows to set a dumping deepness
	 * 
	 * @param unknown_type $number
	 * @return unknown_type
	 */
	public static function setNumOfInstancesBeforeSAS($number)
	{
		self::$numOfInstancesBeforeSASToBrother = (integer) $number;
	}
	
	/**
	 * 
	 * @param unknown_type $actionId
	 * @return unknown_type
	 */
	public static function setSASActionId($actionId)
	{
		self::$sASActionId = (integer) $actionId;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public static function getInputCharSet()
	{
		return self::$inputCharSet;
	}
	
	/**
	 * 
	 * @param unknown_type $str
	 * @return unknown_type
	 */
	public static function setInputCharSet($str)
	{
		self::$inputCharSet = mb_strtoupper((string) $str);
		//also set associated classes char set
		Slug::setInputCharSet(self::$inputCharSet);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getBluePrint()
	{
		return $this->bluePrint;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getLexerInstance()
	{
		if (null === $this->lexerInstance) {
			$this->lexerInstance = LexerRegistry::getInstance(self::$instancesClassName);
		}
		return $this->lexerInstance;
	}
	
	/**
	 * Some actions retrieve their input from child
	 * But as the execution goes from parent to child,
	 * their (second) input may not be available. That
	 * is why they are added to a stack of actions that
	 * await anothers action input.
	 * Once that awaited action is executed its result
	 * will be remembered and injected to the actoins
	 * that are in this stack.
	 * 
	 * @return unknown_type
	 */
	private function addActionIdToActionsAwaitingResult(Thread $t)
	{
		$this->actionIdAwaitingActionIdResult[$t->getAction()->getId()] = $t->getAction()->getOtherInputActionId();
	}
	
	/**
	 * Some actions result is used as input for other actions
	 * When it is the case, a record is created in actionIdAwaitingActionIdResult
	 * so the result of the action can be inputed to those actions
	 * when the moment comes (when they are asked to execute again)
	 * 
	 * @param Thread $t
	 * @return unknown_type
	 */
	private function rememberActionResultForAwaitingActions(Thread $t)
	{
		$this->actionResultForAwaitingActions[$t->getAction()->getId()] = $t->getAction()->getResult();
	}
	
	/**
	 * 
	 * @param unknown_type $actionId
	 * @return unknown_type
	 */
	private function getActionRememberedResult($actionId)
	{
		if (!isset($this->actionResultForAwaitingActions[$actionId])) {
			throw new Exception('There is an action awaiting input from another action, and it is not available');
		}
		return $this->actionResultForAwaitingActions[$actionId];
	}
	
	/**
	 * 
	 * @param unknown_type $t->getAction()Id
	 * @return unknown_type
	 */
	private function hasActionsAwaitingResult($actionId)
	{
		return in_array($actionId, $this->actionIdAwaitingActionIdResult);
	}
	
	/**
	 * 
	 * @param unknown_type $actionId
	 * @return unknown_type
	 */
	private function isAwaitingActionResult($actionId)
	{
		return isset($this->actionIdAwaitingActionIdResult[$actionId]);
	}
	
	/**
	 * Actions are allowed to send messages to engine
	 * in order to make him chage the normal flow
	 * 
	 * @return unknown_type
	 */
	private function recieveMessage(Thread $t)
	{
		$message = $t->getAction()->getMessage();
		if (self::AWAINTINCHILD_RESULT === $message) {
			$this->addActionIdToActionsAwaitingResult($t);
		}
	}
	
	
	/**
	 * 
	 * @return unknown_type
	 */
	private function preExecute()
	{
		
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	private function postExecute()
	{
		
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	private function preClear(Thread $t)
	{
		//handle a possible AWAITINCHILD_RESULT
		if ($this->hasActionsAwaitingResult($t->getAction()->getId())) {
			/*foreach ($this->actionIdAwaitingActionIdResult[$t->getAction()] as $action) {
				$action->injectInput($t->getAction()->getResults());
				$thread = new Thread($action);
				$this->start($thread);//start a new thread with injected input
			}*/
			$this->rememberActionResultForAwaitingActions($t);
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	private function postClear(Thread $t)
	{
		if ($this->isAwaitingActionResult($t->getAction()->getId())) {
			$t->getAction()->injectInput($this->getActionRememberedResult($t->getAction()->getOtherInputActionId()));
			return self::CHANGE_FLOW;
		}
	}
	
	/**
	 * Start calls executeAllResults
	 * So what it does is, to execute all results
	 * generated by an action.
	 * But the call to executeResultsAndBrothers(), makes
	 * it execute every result towards leaf before
	 * goin to the next (child || result)
	 * 
	 * Once execute towards leaf returns REACHED_LEAF
	 * then the action with more (childs || results)
	 * closest to the leaf node is goint to be placed
	 * by : -_clearAndPlaceNEA()
	 * (there is also a case where execution 
	 * fail changes the currentAction
	 * @see clearAndPlaceNIGPA())
	 * 
	 * as currentAction and be executed
	 * towards leaf again etc.
	 * @see executeTowardsLeaf()
	 * 
	 * 
	 * @return unknown_type
	 */
	public function start(Thread $t = null)
	{
		Out::l2("Method Call : Engine::start()\n");
		if (null === $t) {//default start
			$t = new Thread($this->getBluePrint()->getRoot());
		}
		Out::l2('if SAS is used to control dumping deepness then check if sASActionId exists in blue print');
		if (null !== self::$sASActionId) {
			$this->getBluePrint()->getAction(self::$sASActionId);//throws up if does not exist
		}
		$this->executeResultsAndBrothers($t);
		Out::l1("END_OF_BLUEPRINT_EXECUTION\n");
		Out::l2("save remaining instances \n");
		$this->saveInstances();
	}
	
	/**
	 * This function calls executeTowardsLeaf()
	 * and once the leaf has been reached the
	 * clearAndPlaceNEA will set currentAction
	 * to the leaf action's brother, oncle, grand oncle....
	 * which will then execute towards leaf again.
	 * 
	 * 
	 * @return unknown_type
	 */
	private function executeResultsAndBrothers(Thread $t)
	{
		//execute towards leaf until it reaches it
		while (true !== $this->executeTowardsLeaf($t) && self::REACHED_LEAF === $t->getStatus()) {
			//reached leaf, place the next executable action into thread so it is executed towards leaf again
			if (true !== $this->clearAndPlaceNEA($t) && self::NEXT_ACTION_IS_READY_TO_EXECUTE !== $t->getStatus()) {//roll back
				//if the next executable action, is not ready to execute return
				return;
			}
		}
	}

	/**
	 * This executes an action, gets its
	 * child and executes it etc.
	 * The goal is to execute its child
	 * child before its child brothers per say
	 * 
	 * @return unknown_type
	 */
	private function executeTowardsLeaf(Thread $t)
	{
		Out::l2("Method Call : Engine::executeTowardsLeaf()\n");
		do {
			//try to execute action, if it fails rolback to next
			$this->executeCurrentAction($t);
			//action that can be executed and execute it
			if ($t->getStatus() === self::END_OF_BLUEPRINT_EXECUTION) {
				Out::l2("Method Return : Engine::executeTowardsLeaf() : END_OF_BLUEPRINT_EXECUTION\n");
				$t->setStatus(self::END_OF_BLUEPRINT_EXECUTION);
				return;
			} else if (self::SKIP_LEAF === $t->getStatus()) {
				break;//skipping leaf
			}
			//intercept action messages
			if (true === $t->getAction()->hasMessage()) {
				$this->recieveMessage($t);
			}
			//internally it will check whether it has data useful for application
			if (true === $t->getAction()->hasFinalResults()) {
				Out::l1("has final result\n");
				//if it has, it will return it as an associative array
				$this->keepResults($t);
			}
			//remeber the last action for when we get out of the loop
			//while there are more child actions (serially)
			Out::l1("trying to get child action for execution\n");
		} while ($t->getAction()->isReadyToReturnChild() && (true !== $t->setAction($t->getAction()->getChild())));
		Out::l2("Method Return : Engine::executeTowardsLeaf() : REACHED_LEAF\n");
		$t->setStatus(self::REACHED_LEAF);
	}
	
	/**
	 * 
	 * @param Thread $t
	 * @return unknown_type
	 */
	private function keepResults(Thread $t)
	{
		//make sure there is an instance
		if (null === $this->currentInstance) {
			//if not, the content will be spit to the lexer bucket
			$this->actionsNeedingToSpitToLexerBucket[] = $t->getAction();
		}
		//check if need to spit to lexer bucket
		if (false !== array_search($t->getAction(), $this->actionsNeedingToSpitToLexerBucket, true)) {
			//otherwise use the lexer bucket;
			Out::l1("storing data in lexer bucket\n");
			$this->getLexerInstance()->storeInBucket($t->getAction()->spit());
		} else {
			$this->getLexerInstance()->populateInstance($this->currentInstance, $t->getAction()->spit());
		}
	}
	
	/**
	 * This will try to execute currentAction
	 * and will return self::ACTION_EXECUTION_SUCCEED on success...
	 * 
	 * If current action execution fails, it
	 * will ask clearActionAndFindNextExecutableAction() to put
	 * the next executable action in currentAction and try to
	 * execute it by calling itself recursively.
	 * 
	 * It will return self::END_OF_BLUEPRINT_EXECUTION
	 * when the currentAction is the root and it 
	 * fails to execute.
	 * 
	 * @param unknown_type $currentAction
	 * @return unknown_type
	 */
	private function executeCurrentAction(Thread $t)
	{
		Out::l2("Method Call : Engine::executeCurrentAction()\n");
		Out::l2("---------------------------------------------------------\n");
		Out::l1("------------------ ACTION ID : {$t->getAction()->getId()} , TITLE : {$t->getAction()->getTitle()}---------------------\n");
		Out::l1("--------------------" . ($t->getAction() instanceof Extract)? "EXTRACT" : "GETCONTENTS");	
		Out::l2("--------------------\n");
		Out::l1("------------------- PARENT ID : {$t->getAction()->getParent()->getId()}---------------------\n");
		Out::l2("---------------------------------------------------------\n");
		
		/*
		 * make sure the current instance gets saved
		 * and a new one is created when the current
		 * action is NIGP
		 */
		if (true == $t->getAction()->isNewInstanceGeneratingPoint()) {
			$this->manageNIGP($t);
		}
		//execute current action
	 	if (true === ($ret = $t->getAction()->execute())) {
	 		Out::l2("Method Return : Engine::executeCurrentAction() : ACTION_EXECUTION_SUCCEED\n");
	 		$t->setStatus(self::ACTION_EXECUTION_SUCCEED);
	 		return;
	 	} 
	 	$t->setStatus($ret);
		$this->manageExecutionFailAndPlaceNEA($t);
		Out::l2("Method Return : Engine::executeCurrentAction() : self::SKIP_LEAF\n");
		$t->setStatus(self::SKIP_LEAF);
	}
	
	/**
	 * When an action fails to execute or simply does not
	 * execute, there are no results.
	 * The nature of the action execution flow, makes it
	 * impossible for childs to execute as there would be
	 * no input for them to execute on.
	 * 
	 * That is why there is a need to skip the entire
	 * branching of the current action.
	 * 
	 * So what we do is full a clear() (only if it was executed)
	 * to make it look as comming out of blue print.
	 * 
	 * And then we put the the action from which we want
	 * the flow to go again in $this->currentAction
	 * and return self::SKIP_LEAF; which will
	 * tell execute towards leaf that it should not try
	 * to spit any output to currentInstance, but
	 * simply stop executing towards leaf.
	 * Then it will pass the flow to start(), that will
	 * skip the branching and restart the flow from
	 * the action that we placed in currentAction();
	 * 
	 * NEA = NextExecutableAction
	 * 
	 * @return unknown_type
	 */
	private function manageExecutionFailAndPlaceNEA(Thread $t)
	{
		Out::l2("Method Call : Engine::manageExecutionFail() : \n");
		//store error trigger actionId will be used in saveNIGPData
		$this->errorTriggerActionId = $t->getAction()->getId();
	 	Out::l1("current action did not generate any result, id : $this->errorTriggerActionId");
		if (false === $t->getStatus()) {
	 		Out::l1("it was executed, without success, clear(),");
	 		$t->getAction()->clear();
	 		if ($t->getAction()->isOptional()) {
	 			Out::l1("but it is optional, retake flow from parent\n");
	 			$t->setAction($t->getAction()->getParent());
	 		} else {
	 			Out::l1("and it is not optional, rollback to NIGP action\n");
	 			$this->clearAndPlaceNIGPA($t);
	 		}
	 	} else if (AbstractAction::NO_INPUT_IS_OPT_NO_EXEC === $t->getStatus()) {
			"it is opt and it was not executed, so no clear() is needed, retake flow from parent\n";
	 		$t->setAction($t->getAction()->getParent());
		}
		Out::l2("Method Return : Engine::manageExecutionFail() : void\n");
	}
	
	/**
	 * 
	 * NIGP = New Instance Generating Point 
	 * @return unknown_type
	 */
	private function clearAndPlaceNIGPA(Thread $t)
	{
		Out::l2("Method Call : Engine::clearAndPlaceNIGPA()\n");
		while (!$t->getAction()->getParent()->isNewInstanceGeneratingPoint() && !$t->getAction()->getParent()->isRoot()) {
			$this->preClear($t);
			Out::l1("rolling back to new instance generating point last executed child");
			$t->setAction($t->getAction()->getParent());;
		}
		Out::l1("FOUND new instance generating point last executed child");
		Out::l1("clearing Offspring\n");
		$this->preClear($t);
		$t->getAction()->clearMeAndOffspring();
		Out::l1("Placing new instance generating point action in current thread");
		$t->setAction($t->getAction()->getParent());//should ne nIGP
		if (!$t->getAction()->isNewInstanceGeneratingPoint()) {
			Out::l1("WHOOOPSY current action is not new instance generating point wierd!!!");
			throw new Exception('wtf, roll back did not work as expected');
		}
		Out::l2("Method Return : Engine::clearAndPlaceNIGPA() : void");
		$this->nIGPLastInput = array('id' => $t->getAction()->getId(),
									  'data' => $t->getAction()->getLastInput());
	}
	
	/**
	 * This will clear current action and
	 * place the next executable action in
	 * $this->currentAction. It will
	 * return self::CURRENT_ACTION_IS_READY_TO_EXECUTE
	 * to let caller know that it can execute
	 * currentAction.
	 * 
	 * If some action's clear returns 
	 * CANNOT_EXECUTE_AWAITININPUT
	 * it will clear the parent action (by 
	 * calling itself recursively) until
	 * some action can execute or there are
	 * no more actions to execut in bluePrint
	 * in which case it will return 
	 * self::END_OF_BLUEPRINT_EXECUTION
	 * 
	 * NEA= next executable action
	 * @return unknown_type
	 */
	private function clearAndPlaceNEA(Thread $t)
	{
		$this->preClear($t);
		Out::l2("Method Call : Engine::clearAndPlaceNEA(actionId:{$t->getAction()->getId()})\n");
		$ret = $t->getAction()->clear();
		if ($ret === AbstractAction::MORE_CHILDREN_TO_EXECUTE) {
			Out::l1("MORE_CHILDREN_TO_EXECUTE : the current action needs more execution towards leaf\n");
			$t->setAction($t->getAction()->getChild());
		} else if ($ret === AbstractAction::MORE_RESULTS_TO_EXECUTE) {
			Out::l1("MORE_RESULTS_TO_EXECUTE : nothing to do, next call to currentAction->execute() will execute on next result \n");
		} else if ($ret === AbstractAction::CANNOT_EXECUTE_AWAITININPUT) {
			Out::l1("CANNOT_EXECUTE_AWAITININPUT :no more execution for this action\n");
			if (self::CHANGE_FLOW !== $this->postClear($t)) {
				if ($t->getAction()->isRoot()) {
					Out::l1("ROOT : we reached the root while rolling back and its clear method returned cannot exec so no more executions possible.\n");
					Out::l2("Method Return : Engine::clearAndPlaceNEA() : END_OF_BLUEPRINT_EXECUTION\n");
					$t->setStatus(self::END_OF_BLUEPRINT_EXECUTION);
					return;
				} else {
					Out::l1("stepping one action towards root\n");
					$t->setAction($t->getAction()->getParent());
					//this will clear current action as required between each execution
					Out::l2("Method Return : Engine::clearAndPlaceNEA() : call clearAndPlaceNEA()\n");
					$this->clearAndPlaceNEA($t);
					return;
				}
			}
		} else {
			throw new Exception('clear() action method returned unsupported result');
		}
		Out::l2("Method Return : Engine::clearAndPlaceNEA() : NEXT_ACTION_IS_READY_TO_EXECUTE\n");
		$t->setStatus(self::NEXT_ACTION_IS_READY_TO_EXECUTE);
	}
	
	/**
	 * This will create and save instances
	 * when needed
	 * 
	 * NIGP : new instance generating point
	 * 
	 * @return unknown_type
	 */
	private function manageNIGP(Thread $t)
	{
		Out::l2("Method Call : Engine::manageNIGP()\n");
		Out::l1("it is new instance gen point\n");
		//save instances when the stack has reached a certain ceil
		if ($this->numOfInstancesInStackCount >= self::$numOfInstancesInStackBeforeSave) {
			Out::l1("num of instances in stack count > num before save\n");
			$this->saveInstances();
		}
		
		//if using dumping deepness control
		if (0 !== self::$numOfInstancesBeforeSASToBrother) {
			if (null === self::$sASActionId) {
				throw new Exception('you must set the SAS action id if you want to use the dumping deepness control feature');
			}
			//the deepness level has been reached
			if(self::$numOfInstancesBeforeSASToBrother <= $this->numOfInstancesBeforeSASCount) {
				$t->setAction($this->getBluePrint()->getAction((integer)self::$sASActionId));//this will clear all offspring from action with id
				$t->setStatus($t->getAction()->clearMeAndOffspring(false));//this will clear action with id and place it as next executable action in thread (with satuts)
				$this->numOfInstancesBeforeSASCount = 0;//set deepness count to 0
				Out::l1('Using dumping deepness control throug SAS!');
			}

			//count the instance being generated in manageNIGP
			$this->numOfInstancesBeforeSASCount++;
		}
		
		Out::l1("creating new instance\n");
		$this->currentInstance = new self::$instancesClassName();
		//ensure the instance can be saved (it implements method save())
		if (!($this->currentInstance instanceof ActiveRecordInterface)) {
			throw new Exception('Miner_Engine::$instancesClassName class must implement ActiveRecordInterface. ' . print_r(self::$instancesClassName, true) . ' does not implement it.');
		}
		//add it to stack
		Out::l1("add instance to instances array for posterior save\n");
		$this->instancesArray[] = $this->currentInstance;
		$this->numOfInstancesInStackCount++;
		Out::l2("Method Return : Engine::manageNIGP() : void\n");
		
		//spit lexer bucket content if there is one
		$this->getLexerInstance()->injectBucketContent($this->currentInstance);
	}
	
	/**
	 * Save the instances generated during dumping process to the database
	 * 
	 * @return unknown_type
	 */
	private function saveInstances()
	{
		Out::l1("Method Call : Engine::saveInstances()\n");
		foreach ($this->instancesArray as $instance) {
			//Handle eventual exceptions?
			if (true === self::$skipToNextInstanceWhenExceptionOccurs) {
				try {//handle eventual exceptions
					//save all instances
					$instance->save();
					$success = true;
				} catch (Exception $e) {
					Out::l1("Instance save thorwn exception with message : {$e->getMessage()}, skip instance and save next\n");
					if (0 >= $this->numberOfRemainingFailsAllowed) {
						Out::l1('Number of allowed fails with no success, exceeded. Number of subsequent fails allowed : ' . self::$maxExceptionsCatchPerSuccessNumber . ". Remaining fails allowed with no success : {$this->numberOfRemainingFailsAllowed}");
						$this->saveNIGPData();
						exit($e->getMessage());//the script
					}
					$success = false;
				}
				//keep track of the number of exceptions - sucesses difference
				if (true === $success && self::$maxExceptionsCatchPerSuccessNumber > $this->numberOfRemainingFailsAllowed) {//decrease when save() succeed (didn't throw up)
					Out::l1("SAVE SUCCES BUILD UP numberOfRemainingFailsAllowed : {$this->numberOfRemainingFailsAllowed}\n");
					$this->numberOfRemainingFailsAllowed++;
				} else if (false === $success) { // increase when exception was thrown, save() failed
					Out::l1("SAVE FAIL DECREASE numberOfRemainingFailsAllowed : {$this->numberOfRemainingFailsAllowed}\n");
					$this->numberOfRemainingFailsAllowed--;
				}
			//don't handle exceptions
			} else {
				//save all instances
				$instance->save();
			}
		}
		//empty array to avoid saving twice the same instances
		$this->currentInstance = null;
		$this->instancesArray = array();
		$this->numOfInstancesInStackCount = 0;
	}
	
	/**
	 * @todo use this data on next engine launch
	 * 
	 * @return unknown_type
	 */
	private function saveNIGPData()
	{
		if (empty($this->nIGPLastInput)) {
			throw new Exception('The nIGPLastInput array is empty, so cannot save data for next engine start');
		}
		Out::l1("saving nIGP with id :  last input with id : {$this->nIGPLastInput['id']}, data : {$this->nIGPLastInput['data']}, error trigger actionId : $this->errorTriggerActionId");
		if (null === $this->errorTriggerActionId) {
			$this->errorTriggerActionId = 0; //default value
		}
		DbRegistry::getInstance('\\Gbili\\Miner\\Engine\\BluePrint\\Action\\Savable')->saveNIGPLastInputData($this->nIGPLastInput['id'], $this->nIGPLastInput['data'], $this->errorTriggerActionId);
	}
}