<?php
namespace Gbili\Miner\BluePrint\Action\Savable;

use Gbili\Savable\Savable,
    Gbili\Miner\BluePrint,
    Gbili\Miner\BluePrint\Action\Savable\Db\Req,
    Gbili\Miner\BluePrint\Action\Extract\Savable as ExtractSavable,
    Gbili\Miner\BluePrint\Action\GetContents\Savable as GetContentsSavable;

/**
 * This class is not meant for any great work, just to ensure
 * that the action gets all its data. And that it gets saved properly
 * 
 * extending classes must set the 'type' automatically on construction
 * 
 * 
 * @author gui
 *
 */
abstract class AbstractSavable
extends Savable
{

	private static $order = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	const DEFAULT_INPUT_PARENT_REGEX_GROUP_NUMBER = 1;

	/**
	 * Change requestor class name
	 * that will be used by Savable_Savable
	 * when calling method save()
	 * @return unknown_type
	 */
	public function __construct()
	{
		parent::__construct();
		$this->setRequestorClassName(__NAMESPACE__);
	}
	
	/**
	 * This is the action title
	 * 
	 * @param unknown_type $title
	 * @return unknown_type
	 */
	public function setTitle($title)
	{
		$this->setElement('title', (string) $title);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasTitle()
	{
		return $this->isSetKey('title');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getTitle()
	{
		return $this->getElement('title');
	}
	
	/**
	 * When calling this method, child should not have been added
	 * to parent
	 * 
	 * @param unknown_type $parent
	 * @param unknown_type $child
	 * @return unknown_type
	 */
	private function manageOrder($parent, $child)
	{
		//see if parent has a rank
		if (false === ($parentRnk = self::getOrderRank($parent))) {
			if (!empty(self::$order)) {
				throw new Exception('The parent has no rank but some ranks have already been set, you must start adding childs from root cannot create a branch and then add it to trunk' . print_r(array_keys(self::$order), true));
			}
			//this means the parent is root so it is setting itself
			self::$order[] = $parent;
		}
		self::$order[] = $child;
	}
	
	/**
	 * 
	 * @param unknown_type $element
	 * @return unknown_type
	 */
	public static function getOrderRank($element)
	{
		return array_search($element, self::$order, true);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getRank()
	{
		if (false === $rnk = self::getOrderRank($this)) {
			throw new Exception('the rank has not been set for this instance, it means that it was not chained to any parent (or child, when root)');
		}
		return $rnk;
	}
	
	/**
	 * This allows the action to be skipped and all its
	 * children when the parent action being extract did
	 * not generate any input for it
	 * 
	 * @return unknown_type
	 */
	public function setAsOptional()
	{
		$this->setElement('isOpt', true);
	}

	/**
	 * 
	 * @return unknown_type
	 */
	public function getIsOptional()
	{
		if (!$this->isSetKey('isOpt')) {
			$this->setElement('isOpt', false);
		}
		return $this->getElement('isOpt');
	}
	
	/**
	 * You can explicitely instantiate the right class or use this function
	 * to get the class of the type specified in param
	 * 
	 * @param integer $type
	 * @return unknown_type
	 */
	public static function getInstanceOfType($type)
	{
		switch ($type) {
			case BluePrint::ACTION_TYPE_EXTRACT;
				$instance = new ExtractSavable();
				break;
			case BluePrint::ACTION_TYPE_GETCONTENTS;
				$instance = new GetContentsSavable();
				break;
			default;
				throw new Exception('The type you passed is not supported given : ' . print_r($type, true));
				break;
		}
		return $instance;
	}
	
	/**
	 * @TODO the new instance generatig point has a flaw when it is attached to an Extract action with matchAll
	 * we have to hook someplace the new instance generation so that there is an instance for every match in matchall
	 * 1. find at which point matchAll can give a hint on the numer of results.
	 * 2. once we have the number, add some sort of communication between the Extract action, and the Engine::manageNIGP()
	 * 3. make sure that all those instances are saved gracefully. 
	 * @return unknown_type
	 */
	public function setAsNewInstanceGeneratingPoint()
	{
		if (!$this->isSetKey('bluePrint')) {
			throw new Exception('The bluePrint must me set with setBluePrint() before setting the action as newInstanceGeneratingPoint');
		}
		$this->getBluePrint()->setNewInstanceGeneratingPointAction($this);
		$this->setElement('isNewInstanceGeneratingPoint', 1);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function isNewInstanceGeneratingPoint()
	{
		return $this->isSetKey('isNewInstanceGeneratingPoint');
	}
	
	/**
	 * Also sets the element bluePrintId
	 * 
	 * @param BluePrint_Savable $bP
	 * @return unknown_type
	 */
	public function setBluePrint(BluePrint_Savable $bP)
	{
		$this->setElement('bluePrint', $bP);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasBluePrint()
	{
		return $this->isSetKey('bluePrint');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getBluePrint()
	{
		return $this->getElement('bluePrint');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function isRoot()
	{
		return ($this->getType() === BluePrint::ACTION_TYPE_GETCONTENTS && !$this->isSetKey('parentAction'));
	}
	
	/**
	 * 
	 * @param $parentId
	 * @return unknown_type
	 */
	public function setParent(AbstractSavable $action)
	{
		$this->setElement('parentAction', $action);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasParent()
	{
		return $this->isSetKey('parentAction');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getParent()
	{
		return $this->getElement('parentAction');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function addChild(AbstractSavable $action)
	{
		$action->setBluePrint($this->getBluePrint());
		$action->setParent($this);
		//this will set the rank of the parent and child
		$this->manageOrder($this, $action);
		$this->useKeyAsArrayAndPushValue('childAction', $action, Savable::POST_SAVE_LOOP);
	}
	
	/**
	 * 
	 * @param BluePrint_Action_Savable_Abstract $action
	 * @param unknown_type $inputGroup
	 * @return unknown_type
	 */
	public function injectResultTo(AbstractSavable $action, $inputGroup = null)
	{
		if (null !== $inputGroup) {
			if (!($this instanceof ExtractSavable)) {
				throw new Exception('You cannot set the input group if the injecting action is not of type extract');
			}
			$action->setInjectInputGroup($inputGroup);
		}
		$this->setElement('injectedAction', $action);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function injectsAction()
	{
		return $this->isSetKey('injectedAction');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getInjectedAction()
	{
		return $this->getElement('injectedAction');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasInjectInputGroup()
	{
		return $this->isSetKey('injectInputGroup');
	}
	
	/**
	 * 
	 * @param unknown_type $group
	 * @return unknown_type
	 */
	public function setInjectInputGroup($group)
	{
		$this->setElement('injectInputGroup', (integer) $group);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getInjectInputGroup()
	{
		return $this->getElement('injectInputGroup');
	}

	/**
	 * 
	 * @return array of BluePrint_Action_Savable_Abstract
	 */
	public function getChildren()
	{
		return $this->getElement('childAction');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasChildren()
	{
		return $this->isSetKey('childAction');
	}

	/**
	 * 
	 * @param $inputParentRegexGroupNumber
	 * @return unknown_type
	 */
	public function setInputParentRegexGroupNumber($inputParentRegexGroupNumber)
	{
		$this->setElement('inputParentRegexGroupNumber', (integer) $inputParentRegexGroupNumber);
	}
	
	/**
	 * Sets the input group to default when the parent is of type extract
	 * otherwise it sets the input group to no group specified in requestor
	 * 
	 * 
	 * @return unknown_type
	 */
	public function getInputParentRegexGroupNumber()
	{
		if (!$this->isSetKey('inputParentRegexGroupNumber')) {
			if ($this->hasParent() && $this->getParent()->getType() === BluePrint::ACTION_TYPE_EXTRACT) {
				$this->setInputParentRegexGroupNumber(self::DEFAULT_INPUT_PARENT_REGEX_GROUP_NUMBER);
			} else {
				$this->setInputParentRegexGroupNumber(Req::DEFAULT_NO_INPUT_PARENT_REGEX_GROUP_NUMBER);
			}
		}
		return $this->getElement('inputParentRegexGroupNumber');
	}
	
	/**
	 * 
	 * @param unknown_type $data
	 * @return unknown_type
	 */
	public function setData($data)
	{
		$this->setElement('data', (string) $data);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getData()
	{
		return $this->getElement('data');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasData()
	{
		return $this->isSetKey('data');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getType()
	{
		return $this->getElement('type');
	}
}