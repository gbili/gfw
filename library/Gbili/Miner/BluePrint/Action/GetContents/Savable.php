<?php
namespace Gbili\Miner\BluePrint\Action\GetContents;

use Gbili\Miner\BluePrint\Action\Savable\AbstractSavable, 
    Gbili\Miner\BluePrint\Action\CMLoader, 
    Gbili\Miner\BluePrint;

/**
 * 
 * @author gui
 *
 */
class Savable
extends AbstractSavable
{
	/**
	 * 
	 * @return unknown_type
	 */	
	public function __construct()
	{
		parent::__construct();
		$this->setElement('type', BluePrint::ACTION_TYPE_GETCONTENTS);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasCallbackMethod()
	{
		return $this->isSetKey('callbackMethod');
	}
	
	/**
	 * 
	 * @param unknown_type $methodName
	 * @return unknown_type
	 */
	public function setCallbackMethod($methodName)
	{
		if (!is_string($methodName)) {
			throw new Exception('the method name must be passed as a string');
		}
		if (!$this->hasBluePrint()) {
			throw new Exception('The bluePrint must be set in order tu map the action to its callback method');
		}
		if ($this->getBluePrint()->hasCallbackPath()) {
			$path = $this->getBluePrint()->getCallbackPath();
			$type = CMLoader::PATH_TYPE_DIRECT;
		} else if ($this->getBluePrint()->hasBasePath()) {
			$path = $this->getBluePrint()->getBasePath();
			$type = CMLoader::PATH_TYPE_BASE;
		} else {
			throw new Exception('There is no way to find the callback class if no path is provided in bluePrint');
		}
		if (!is_string(($className = CMLoader::loadCallbackClass($path, $this->getBluePrint()->getHost(), $type)))) {
			throw new Exception('the class could not be loaded errors : ' . print_r(CMLoader::getErrors(), true));
		}
		if (false === CMLoader::methodExists($className, $methodName)) {
			throw new Exception("the method '$methodName' does not exist in $className");
		}
		$this->setElement('callbackMethod', $methodName);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getCallbackMethod()
	{
		return $this->getElement('callbackMethod');
	}
	
	/**
	 * 
	 * @param array $mapping
	 * @return unknown_type
	 */
	public function setCallbackMap(array $mapping)
	{
		if (array_keys($mapping) !== range(0, count($mapping) - 1)) {
			throw new Exception('Mapping not supported, keys should range from 0 to n');
		}
		$this->setElement('callbackMapping', $mapping);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getCallbackMap()
	{
		return $this->getElement('callbackMap');
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasCallbackMap()
	{
		if ($ret = $this->isSetKey('callbackMap')) {
			$ret = $this->getElement('callbackMap');
			$ret = !empty($ret);//ensure not empty array
		}
		return $ret;
	}
}