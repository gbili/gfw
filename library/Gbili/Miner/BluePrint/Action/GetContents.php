<?php
namespace Gbili\Miner\BluePrint\Action;

use Gbili\Miner, 
    Gbili\Out\Out,
    Gbili\Url\Url, 
    Gbili\Encoding\Encoding;

/**
 * Get the contents from a url and
 * converts the output to utf8 if needed
 * 
 * @author gui
 *
 *
 */
class GetContents
extends AbstractAction
{
		
	/**
	 * 
	 * @var array
	 */
	private static $secondsDelayBetweenRequests = array(10, 25);
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $callbackWrapper = null;

	/**
	 * Contains the input data when the action
	 * is the root actio and it cannot retrieve
	 * the input from another action.
	 * 
	 * Once used, this var is set to false
	 * 
	 * @var string
	 */
	private $bootstrapInputData = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $result = null;
	
	/**
	 * 
	 * @param allows to set the input data from construction 
	 * (usefull from bluePrint when root action) $urlString
	 * @return unknown_type
	 */
	public function __construct($urlString = null)
	{
		//this is a hack to let the root action take its input from itself
		if (null !== $urlString) {
			$this->bootstrapInputData = $urlString;
		}
	}
	

	
	/**
	 * 
	 * @param integer $min
	 * @param integer $max
	 * @return unknown_type
	 */
	public static function setSecondsDelayBetweenRequests($min, $max = null)
	{
		if (!is_int($min)) {
			throw new GetContents\Exception('setSecondsDelay, min parameter must be of type int');
		}
		if (null !== $max) {
			if (!is_int($max)) {
				throw new GetContents\Exception('setSecondsDelay, max parameter must be of type int');
			}
			if ($min > $max) {
				throw new GetContents\Exception("max must be at least equal to min or greater given : min $min, max $max");
			}
			self::$secondsDelayBetweenRequests = array($min, $max);
		} else {
			self::$secondsDelayBetweenRequests = $min; 
		}
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public static function getSecondsDelayBetweenRequests()
	{
		return (is_array(self::$secondsDelayBetweenRequests))? rand(self::$secondsDelayBetweenRequests[0], self::$secondsDelayBetweenRequests[1]) : self::$secondsDelayBetweenRequests;
	}
	
	/**
	 * 
	 * @param Miner_Engine_BluePrint_Action_GetContents_Callback_Wrapper $cW
	 * @return unknown_type
	 */
	public function setCallbackWrapper(GetContents\Callback\Wrapper $cW)
	{
		$this->callbackWrapper = $cW;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getCallbackWrapper()
	{
		if (null === $this->callbackWrapper) {
			throw new GetContents\Exception('The callback handler is not set');
		}
		return $this->callbackWrapper;
	}
	
	/**
	 * This type of action never have final results
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#hasFinalResults()
	 */
	public function hasFinalResults()
	{
		return false;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#getResult($groupNumber)
	 */
	public function getResult($groupNumber = null)
	{
		if (false === $this->isExecuted) {
			throw new GetContents\Exception('You must call execute() before getResult()');
		}
		return $this->result;
	}
	
	/**
	 * 
	 * @param unknown_type $action
	 * @return unknown_type
	 */
	private function getInputFromAction($action, $groupNumber)
	{
		$input = null;
		if (!($action instanceof Extract)) {
			throw new GetContents\Exception("GetContents parent must be of type extract when not root");
		}
		
		if ($action->isExecuted()) {
			//see whether there is a callback used to treat input
			if (null !== $this->callbackWrapper) {
				$input = $action->getResults();
			//otherwise make sure it is possible directly take input as a string
			} else if (null !== $groupNumber) {
				$input = $action->getResult($groupNumber);	
			} else {
				throw new GetContents\Exception("GetContents action needs a string as input. Array is allowed only when using callback. As it is not the root action you must specify the groupForInputData so it can take a single element from the parent extract element");
			}
		}
		return $input;
	}
	
	/**
	 * The input can come from three different places
	 * When root : 
	 * -boostratInputData
	 * Else :
	 * -other action than parent : $otherInputActionId & $otherActionGroupForInputData
	 * -lastInput (in case there is a cw) : $lastInput 
	 * -parent : $inputGroupNumber
	 * 
	 * The normal action flow is that the roots gets input from bostrapInputData
	 * then it executes, and the result is made available for the children
	 * Then the child executes and so on.
	 * However there may be cases, where some action will need to take
	 * input from a child action so it can create more results that is when
	 * the flow chages for some loops, until the child cannot generate more
	 * results.
	 * 
	 * @return unknown_type
	 */
	private function retrieveInput()
	{
		$input = null;
		//if it is the root
		if ($this->isRoot()) {
			if (null === $this->bootstrapInputData) {//make sure input is available
				throw new GetContents\Exception("It is root and it has no input data when root");
			} else if (false !== $this->bootstrapInputData) {//make sure it is the first time execute is called
				$ret = $this->bootstrapInputData;
				$this->bootstrapInputData = false; //make sure the bootsrap input does not get inputed more than once, after first time, use lastInput
				return $ret;
			}
		}
		//if it still has no input, it means it has already been executed or it is not the root
		//see whether it can take the input from another action than its parent when available
		if (null !== $this->otherInputActionId) {
			//make sure result is still available
			return $this->getInputFromAction($this->getBluePrint()->getAction($this->otherInputActionId), $this->otherActionGroupForInputData);
			
		}
		//if input is still not available
		//otherwise see if input is available from callback last result or first execution result
		if (null !== $this->lastInput && null !== $this->callbackWrapper) {
			return $this->lastInput;
		} else if (! $this->isRoot()) { //otherwise input must be taken from parent
			return $this->getInputFromAction($this->getParent(), $this->groupForInputData);
		} else {
			throw new GetContents\Exception('The action could not retrieve its content from any of the options available');
		}
	}
	
	/**
	 * This will allow the engine to inspect the
	 * actions last input
	 * 
	 * @return unknown_type
	 */
	public function getInput()
	{
		return $this->retrieveInput();
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	protected function execute()
	{
		if (true === $this->isExecuted) {
			throw new GetContents\Exception("call clear before reexecuting");
		}
		
		//let the engine know that it needs to execute this action
		//again, once the result is avaiable from child
		if (null !== $this->otherInputActionId) {
			$this->message = Engine::AWAITINCHILD_RESULT;
		}

		$input = $this->retrieveInput();

		//allow for callback to intercept the input
		if (null !== $this->callbackWrapper) {
			if (!$this->callbackWrapper->hasMoreLoops()) {
				throw new GetContents\Exception("loop reached end cannot execute anymore, call clear()");
			}
			Out::l2('applying callback to input' . "\n");
			Out::l2("input is $input \n");
			$input = $this->callbackWrapper->apply($input);
			Out::l2("input after callback apply is $input \n");
			Out::l2('saving callback result for next execute() call' . "\n");
			//remember the last output of callback so it does not output the same result everytime
			//note that calling callback->rewindLoop in this case will directly set the loop to end
			//on next call because the input wont set itself back to the first state
		}
		$this->lastInput = $input;

		//generate the result
		Out::l2("create the url with input : $input \n");
		$url = new Url($input);
		if (!$url->isValid()) {
			throw new GetContents\Exception("the url string is not valid given : " . print_r($url));
		}
		
		//obfuscation
		$this->applyDelay();
		
		//save last input so it is available when some action crashes, this will help the engine start next time
		$this->result = @file_get_contents($url->toString());
		if (false === $this->result) {
			throw new GetContents\Exception('file_get_contents() did not succeed, url : ' . print_r($url->toString(), true));
		}
		Out::l2('adjusting charset to utf8 if needed, current result charset : ' . Encoding::detectEncoding($this->result) . "\n");
		$this->result = Encoding::utf8Encode($this->result);
		print_r(mb_substr(htmlentities($this->result), 0, 50));
		Out::l2('execution succeed' . "\n");
		return $this->isExecuted = true;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#clear()
	 */
	protected function clear()
	{
		Out::l2("GET_CONTENTS : clear() \n");
		
		$this->isExecuted = false;
		$this->result = null;
		
		Out::l2("GET_CONTENTS : has callback wrapper? : ");
		//if callback is being used
		if (null !== $this->callbackWrapper) {
			Out::l2("yes! \n");
			//skip this check if full clear
			//and from the same input, it can create more results
			Out::l2("GET_CONTENTS : wrapper has more loops? : ");
			if ($this->callbackWrapper->hasMoreLoops()) {
				Out::l2("yes! \n");
				return AbstractAction::MORE_RESULTS_TO_EXECUTE;//tell engine to call execute again.
			}
			Out::l2("no! \n");
			Out::l2("GET_CONTENTS : action is root? : ");
			//otherwise if it cannot create more results from the same input
			if (!$this->isRoot()) {//and it is not root
				Out::l2("no! so rewind loop and clear input data so it can get new input from parent next time \n");
				$this->callbackWrapper->rewindLoop();//rewind the loop and wait for new input
				//now, inputDataWhenRoot can also be used as a placeholder for callback output
				//as it is allways used as input when not null, make sure it is null so on next
				//execution the input will be taken from the parent new result
				$this->bootstrapInputData = null;//empty placeholder
			}
		}
		Out::l2("no! \n");
		Out::l2("GET_CONTENTS : cannot execute waiting parent execute\n");
		//and tell the parent to execute parent
		return AbstractAction::CANNOT_EXECUTE_AWAITININPUT;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Engine/BluePrint/Action/AbstractAction#_fullClear()
	 */
	protected function fullClear()
	{
		$this->isExecuted = false;
		$this->result = null;
		if (null !== $this->callbackWrapper) {
			$this->callbackWrapper->rewindLoop();
			$this->bootstrapInputData = null;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#spit()
	 */
	public function spit()
	{
		throw new GetContents\Exception('GetContents actions never have final results, don\'t call spit().');
	}
	
	/**
	 * This helps in obfuscation
	 * 
	 * @return unknown_type
	 */
	private function applyDelay()
	{
		$delay = self::getSecondsDelayBetweenRequests();
		Out::l2("delaying request for $delay seconds");
		for ($i = 0; $i < $delay; $i++) {
			sleep(1);
			Out::l2('.');
		}
		Out::l2("\n");
	}
}