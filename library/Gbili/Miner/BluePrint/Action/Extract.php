<?php
namespace Gbili\Miner\BluePrint\Action;

use Gbili\Out,
    Gbili\Regex;


/**
 * This class is meant to extract meaningfull data (data associated to its meaning)
 * from input data fetched from it's parent
 * 
 * @author gui
 *
 */
class Extract
extends AbstractAction
{	
	/**
	 * Final result optional or required
	 * @var unknown_type
	 */
	const FR_OPTIONAL = 213;
	const FR_REQUIRED = 214;
	
	/**
	 * The regex use in the preg_match function
	 * 
	 * @var Miner_Engine_Regex_String_Abstract
	 */
	private $regexStr;
	
	/**
	 * Tells whether the array contained in $results
	 * must be used for muyltiple Video Entities
	 * (the results must not be erased) completely from
	 * the function: clear(), instead just the first record
	 * must be shifted.
	 * 
	 * @var boolean
	 */
	private $useMatchAll;
	
	/**
	 * Contains the regex instance from where all results are fetched
	 * 
	 * @var Regex
	 */
	private $regex;
	
	/**
	 * Maps each group of the regex, to an entity
	 * (a final result)
	 * @var unknown_type
	 */
	private $groupToEntityArray;
	
	/**
	 * Tells if getResult() can be called
	 * 
	 * @var unknown_type
	 */
	private $isResultReady;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $hasFinalResults = false;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $hasChildAction = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $currentChildAction = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $nextStep = null;
	
	/**
	 * Contains the method wrapper that will
	 * intercept the results
	 * 
	 * @var unknown_type
	 */
	private $methodWrapper = null;
	
	/**
	 * Allows to avoid intercepting results twice
	 * Stores the intercepted results untill clear is called
	 * 
	 * @var unknown_type
	 */
	private $interceptedResults = null;
	
	/**
	 * 
	 * @param $regex is the regular expression needed to extract the content from the inputData
	 * @param $useMatchAll
	 * @param $groupNumToEntityArray
	 * @param $nextActionInputDataGroupNumber
	 * @return unknown_type
	 */
	public function __construct($regexStr, $useMatchAll = false)
	{
		parent::__construct();
		if (true === is_string($regexStr)) {
			$regexStr = new \Gbili\Regex\String\Generic($regexStr);
		}
		if (!($regexStr instanceof \Gbili\Regex\String\AbstractString)) {
			throw new Extract\Exception('Action extract constructor first parameter must be of type string or instance of Regex_String_Abstract');
		}
		$this->regexStr = $regexStr;
		$this->useMatchAll = (boolean) $useMatchAll;
		//prepare the intercept hook
		$this->methodInterceptGroupMapping[Extract\Method\Wrapper::INTERCEPT_TYPE_TOGETHER] = array();
		$this->methodInterceptGroupMapping[Extract\Method\Wrapper::INTERCEPT_TYPE_ONEBYONE] = array();
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function setMethodWrapper(Extract\Method\Wrapper $mW)
	{
		$this->methodWrapper = $mW;
	}
	
	/**
	 * Maps each group in the regex to an entity in the application
	 * ex: array(2=>array(VideoName, self::FR_OPTIONAL, 'explode'), 3=>DateReleased);
	 * 
	 * @param $groupToEntity
	 * @return unknown_type
	 */
	public function setGroupMapping(array $groupToEntityArray)
	{
		//if an empty array is passed, it means there will be just one group
		//and it will be used as input data for the child action
		if ($this->hasFinalResults = !empty($groupToEntityArray)) {
			$this->groupToEntityArray = $groupToEntityArray;
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#hasFinalResults()
	 */
	public function hasFinalResults()
	{
		if (null === $this->hasFinalResults) {
			throw new Extract\Exception('You must call setGroupMapping() before hasFinalResults()');
		}
		return $this->hasFinalResults;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#spit()
	 */
	public function spit()
	{
		if (false === $this->hasFinalResults) {
			throw new Extract\Exception('This action has no final results, therefor it cannot spit, call hasFinalResults() before calling spit() to avoid exception.');
		}
		if (false === $this->isResultReady) {
			throw new Extract\Exception('The result is not ready, call execute()');
		}
		//get rid of results that are not final in results array (getResults())
		$entityToResult = array();
		$results = $this->getResults();
		//map each entity to its result
		Out::l2("Spitting results: \n");
		Out::l2("Results array after extract and intercept process : \n");
		foreach ($this->groupToEntityArray as $k => $array) {
			if (isset($results[$array['regexGroup']]) && !empty($results[$array['regexGroup']])) {
				$entityToResult[$array['entity']] = $results[$array['regexGroup']];
			} else if (0 === $array['isOpt']) {
				throw new Extract\Exception('There is a result that is required and it is missing : ' . print_r($results, true) . ' mapping : ' . print_r($this->groupToEntityMapping));
			}
		}
		Out::l2("Results array after associating them to entities : ");
		return $entityToResult;
	}
	
	/**
	 * Make regex object contain some results
	 * 1. Has been executed
	 * 		a. is not MatchAll -> throw up (only one execution per result)
	 * 		b. is MatchAll	   -> advance the pointer to the next result so getResult() can return it
	 * 2. Has not been executed
	 * 		a. is MatchAll
	 * 		b. is not MatchAll
	 * 
	 * @note isResultReady replaces isExecuted
	 * @return boolean
	 */
	protected function execute()
	{
		//if has never been executed
		if (false === $this->isExecuted) {
			Out::l2("first time execute is called\n");
			//create new regex instances
			//PARENT : EXTRACT specify group for input
			if ($this->getParent() instanceof self) {
				Out::l2("parent is of type Extract so get the result by passing the group number\n");
				if (null === $this->groupForInputData) {
					throw new Exception('call setGroupForInputData($group), when the parentAction is an instance of Extract');
				}
				$this->regex = new Regex($this->getParent()->getResult($this->groupForInputData), $this->regexStr);
			//PARENT : GETCONTENTS
			} else {
				Out::l2("parent is of type GetContents so get the result without passing the group number\n");
				//no need to specify the group for intput data
				$this->regex = new Regex($this->getParent()->getResult(), $this->regexStr);
			}
			
			//save last input
			$this->lastInput = $this->regex->getInputString();
			Out::l2('input first 50/' . mb_strlen($this->lastInput) . ' chars are : ' . mb_substr($this->lastInput, 0, 100));
			Out::l2('regex is : ' . $this->regexStr->toString());
			
			//call the right function (preg_match / preg_match_all)
			if (true === $this->useMatchAll) {
				Out::l2("is useMatchAll, call regex->matchAll()\n");
				$this->isResultReady = (boolean) $this->regex->matchAll();
			} else {
				Out::l2("is not useMatchAll, call regex->match()\n");
				$this->isResultReady = (boolean) $this->regex->match();
			}
			//now it was executed at least once
			$this->isExecuted = true;
			Out::l2("remember that execute was called at least once\n");
		//else if has already been executed at least once
		} else {
			Out::l2("this Extract instance was already executed once\n");
			//make sure clear() was called between two execute() calls
			if (false === $this->useMatchAll) {
				throw new Extract\Exception('You are trying to execute the same action twice whereas it is not useMatchAll call clear()');
			}
			Out::l2("it is use match all and the result points to the right one so result is ready\n");
			$this->isResultReady = true;
		}
		Out::l2('Input was : ' . print_r($this->regexStr->getFullRegex(), true));
		//let the calling class know if everything went ok
		return $this->isResultReady;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#getResult($groupNumber)
	 */
	public function getResult($groupNumber = null)
	{
		if (null === $groupNumber) {
			throw new Extract\Exception('The group number cannot be null (getResult() first param)');
		}
		$res = $this->getResults();
		//ensure the group is in the regex result array
		if (!isset($res[$groupNumber])) {
			throw new Extract\Exception('The group does not exist in regex object');
		}
		//return the specified group
		return $res[$groupNumber];	
	}
	
	/**
	 * 
	 * @param unknown_type $groupNumber
	 * @return unknown_type
	 */
	public function hasGroupNumber($groupNumber)
	{
		//ensure there is a result
		if (false === $this->isResultReady) {
			throw new Extract\Exception('You must call execute() before getResult(), or the regex is not valid.');
		}
		return $this->regex->hasGroupNumber($groupNumber);
	}
	
	/**
	 * 
	 * @param unknown_type $groupNumber
	 * @return unknown_type
	 */
	public function hasResult($groupNumber)
	{
		return $this->hasGroupNumber($groupNumber);
	}
	
	/**
	 * Return all the results
	 * 
	 * @return unknown_type
	 */
	public function getResults()
	{	
		//ensure there is a result
		if (false === $this->isResultReady) {
			throw new Extract\Exception('You must call execute() before getResult(), or the regex is not valid.');
		}
		//return all groups of current match, if match all only get current match
		/*
		 * Allow the user to intercept the results and modify them with some method
		 */
		if (null === $this->interceptedResults) {
			$this->interceptedResults = $this->regex->getCurrentMatch();	
			if (null !== $this->methodWrapper) {
				$this->interceptedResults = $this->methodWrapper->intercept($this->interceptedResults);
			}
		}
		return $this->interceptedResults;
	}
	
	/**
	 * On one hand clear is used to handle the regex results,
	 * 	in the case it is NOT matchAll : 
	 * 		it will null the regex so a new regex will be generated in execute() with a new input string from the parent result
	 * 	in the case it is matchAll :
	 * 		if there are more matches in stack
	 * 			it will try to point to the next match so it can be returned by getResults
	 * 		if there are no more matches in stack
	 * 			it will null the regex so a new regex will be generated by execute() with a new input string from parent result
	 * On the other hand it handles the child 
	 * 	if it is a stack
	 * 		when there are more childs in stack it will throw an exception by telling the instance cannot be cleared untill all childs have been executed
	 * 		when there are NO more childs in stack it will clear the instance normally and set the shiftedChildActions back to childActions so the can be executed again with new data
	 * 	if it is NOT stack it will clear the instance normally
	 * 
	 * (non-PHPdoc)
	 * @see BluePrint/Miner_Engine_BluePrint_Action#clear()
	 */
	protected function clear()
	{		
		//empty intercepted results so next call will populate them with fresh data
		$this->interceptedResults = null;
		
		//see if there are more results to be executed
		if (true === $this->useMatchAll && true === $this->regex->goToNextMatch()) {
			Out::l2("EXTRACT: MORE_RESULTS_TO_EXECUTE");
			//result is ready
			return AbstractAction::MORE_RESULTS_TO_EXECUTE;
		}

		//forbid getResult() to be called before execute()
		//and forbid execute to be called twice without calling clear() in between
		$this->isResultReady = false;
		$this->isExecuted = false;
		$this->regex = null;
		return AbstractAction::CANNOT_EXECUTE_AWAITININPUT;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Engine/BluePrint/Action/AbstractAction#_fullClear()
	 */
	protected function fullClear()
	{
		$this->interceptedResults = null;
		$this->isResultReady = false;
		$this->isExecuted = false;
		$this->regex = null;
	}
}