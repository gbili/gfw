<?php
namespace Gbili\Miner\BluePrint\Action;

use Gbili\Miner\BluePrint,
    Gbili\Out\Out;

/**
 * This will contain an object representation of
 * the action table set.
 * The actions will inject themselves the input data
 * from their parents
 * They will also know what part of the result means
 * what, and will make the final results (if there is any)
 * be available from the public method spit().
 * 
 * 
 * @author gui
 *
 */
abstract class AbstractAction
{

	const MORE_CHILDREN_TO_EXECUTE = 21;
	const MORE_RESULTS_TO_EXECUTE = 22;
	const CANNOT_EXECUTE_AWAITININPUT = 23;
	const NO_INPUT_IS_OPT_NO_EXEC = 41;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $id = null;
	
	/**
	 * 
	 * @var BluePrint
	 */
	private $bluePrint = null;
	
	/**
	 * Lets the Miner_Engine_Engine know whether
	 * it has to create a new instance of
	 * Miner_Engine::$instancesClassName
	 * 
	 * @todo integrte it
	 * @var unknown_type
	 */
	private $isNewInstanceGeneratingPoint = null;
	
	/**
	 * Keep a pointer of the root action
	 * so it can be accessed quicker
	 * than by doing a loop and calling 
	 * getParent on all the actions chain...
	 * 
	 * @var AbstractAction
	 */
	private $rootAction = null;
	
	/**
	 * Points to the parent action which
	 * may be itself in case it is the root
	 * 
	 * @var AbstractAction
	 */
	private $parentAction = null;
	
	/**
	 * If false means it is the leaf action
	 * 
	 * @var AbstractAction
	 */
	private $childrenActionStack = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $hasChildAction = null;
	
	/**
	 * When go to next child action
	 * reaches the end of childrenActionStack()
	 * this is true;
	 * 
	 * @var unknown_type
	 */
	private $needRewindChildrenStack = false;
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $currentChild = null;
	
	/**
	 * The parent result group from which the input is taken from
	 * 
	 * @var integer
	 */
	private $takeInputFromGroupNumber = null;
	
	/**
	 * The group in the parent results from which
	 * this object will get its input data when
	 * callin getParent()->getResult(_groupForInputData)
	 * 
	 * @var integer
	 */
	protected $groupForInputData = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected $isOpt = false;
	
	/**
	 * Gives humans some insight about the action
	 * 
	 * @var unknown_type
	 */
	private $title = null;
	
	/**
	 * Whenever execute() is called this should
	 * be set to true and clear() should set it
	 * to false
	 * 
	 * @var boolean
	 */
	protected $isExecuted = false;
	
	/**
	 * This will be saved by the engine
	 * in case it is the new instance
	 * generating point to let him know
	 * where to start the dumping process
	 * next time
	 * 
	 * It does never get cleared, so
	 * if the parent action gives some
	 * input it should allways be set
	 * 
	 * 
	 * @var unknown_type
	 */
	protected $lastInput = null;
	
	/**
	 * This allows the action to store messages
	 * that can be retrieved by te engine
	 * in order to change flow or behaviour
	 * @var unknown_type
	 */
	protected $message = null;
	
	/**
	 * 
	 * @var unknown_type
	 */
	protected $otherActionInput = null;
	
	/**
	 * Contains the action id from
	 * which this action will take
	 * input once it is available
	 * 
	 * @var unknown_type
	 */
	protected $otherInputActionId = null;
	
	/**
	 * Group number for input from other action
	 * 
	 * @var unknown_type
	 */
	protected $otherActionGroupForInputData = null;
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function __construct()
	{
		
	}
	
	/**
	 * 
	 * @param unknown_type $id
	 * @return unknown_type
	 */
	public function setId($id)
	{
		$this->id = (integer) $id;
	}
	
	/**
	 * Give some human insight about the action
	 * 
	 * @param unknown_type $title
	 * @return unknown_type
	 */
	public function setTitle($title)
	{
		$this->title = (string) $title;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasTitle()
	{
		return '' !== $this->title;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getId()
	{
		if (null === $this->id) {
			throw new Exception('there is no id set for this action');
		}
		return $this->id;
	}
	
	/**
	 * 
	 * @param unknown_type $input
	 * @return unknown_type
	 */
	public function injectInput($input)
	{
		$this->otherActionInput = $input;
	}
	
	/**
	 * Returns the action that should 
	 * be used as input when available
	 * 
	 * @return unknown_type
	 */
	public function getOtherInputActionId()
	{
		if (null === $this->otherInputActionId) {
			throw new Exception("There is no other input actionId");
		}
		return $this->otherInputActionId;
	}
	
	/**
	 * Returns the action that should 
	 * be used as input when available
	 * 
	 * @return unknown_type
	 */
	public function setOtherInputActionInfo($id, $groupForInputData = null)
	{
		$this->otherInputActionId = (integer) $id;
		$this->otherActionGroupForInputData = $groupForInputData;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getBluePrint()
	{
		if (null === $this->bluePrint) {
			throw new Exception('Blue print was not set');
		}
		return $this->bluePrint;
	}
	
	/**
	 * 
	 * @param BluePrint $b
	 * @return unknown_type
	 */
	public function setBluePrint(BluePrint $b)
	{
		$this->bluePrint = $b;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getLastInput()
	{
		return $this->lastInput;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasLastInput()
	{
		return null !== $this->lastInput;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasMessage()
	{
		return $this->message !== null;
	}

	/**
	 * 
	 * @return unknown_type
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * 
	 * @return unknown_type
	 */
	public function isRoot()
	{
		if (null === $this->rootAction) {
			throw new Exception('There is not root action set for this action so you cannot call isRoot()');
		}
		return ($this === $this->rootAction);
	}
	
	/**
	 * This function should only be called from within the setChildAction function
	 * 
	 * @param AbstractAction $action
	 * @return unknown_type
	 */
	public function setParent(AbstractAction $action, $setter)
	{
		if (!($setter instanceof self)) {
			throw new Exception('This function should only be called from AbstractAction and subclasses');
		}
		$this->parentAction = $action;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getParent()
	{
		if (null === $this->parentAction) {
			throw new Exception('there is no parent action set right now, call setParent() before calling getParent()');
		}
		return $this->parentAction;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function setAsRoot()
	{
		if ($this->rootAction !== null) {
			throw new Exception('The root  has already been set');
		}
		$this->rootAction = $this;
		$this->setParent($this, $this);
	}
	
	/**
	 * 
	 * @param AbstractAction $action
	 * @return unknown_type
	 */
	public function setRoot(AbstractAction $action, $setter)
	{
		if (!($setter instanceof self)) {
			throw new Exception('Lock is wrong, setRoot() should only be called from within setChild() function');
		}
		$this->rootAction = $action;
	}
	
	/**
	 * Returns the pointer to the root action
	 * 
	 * @return unknown_type
	 */
	public function getRoot()
	{
		//if there is no root action, it is the root action
		if (null === $this->rootAction) {
			throw new Exception('The root action is not set');
		}
		return $this->rootAction;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function isExecuted()
	{
		return $this->isExecuted;
	}
	
	/**
	 * When there is at least one child
	 * and clear() has has not outdated
	 * it yet, the child can be returned
	 * 
	 * @return unknown_type
	 */
	public function isReadyToReturnChild()
	{
		return ($this->hasChildren() && null !== $this->currentChild && !$this->needRewindChildrenStack());
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getChild()
	{
		if (!$this->isReadyToReturnChild()) {
			throw new Exception("There is no child, or the children stack arrived to the end, call clear() to renew stack");
		}
		return $this->currentChild;
	}
	
	/**
	 * has the action has any children?
	 * 
	 * @return unknown_type
	 */
	public function hasChildren()
	{
		return !empty($this->childrenActionStack);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getChildren()
	{
		return $this->childrenActionStack;
	}
	
	/**
	 * Populates currentChild
	 * If there childrenActionStack array
	 * pointer exceeds the last element
	 * it will return false
	 * It will also return false if there are
	 * no children
	 * 
	 * @return unknown_type
	 */
	public function goToNextChildInStack()
	{
		if ($this->hasChildren()) {
			list($k, $this->currentChild) = each($this->childrenActionStack);
			return !($this->needRewindChildrenStack = (null === $this->currentChild));
		}
		return false;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function needRewindChildrenStack()
	{
		return $this->needRewindChildrenStack;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function rewindChildrenStack($skipCheck = false)
	{
		if ($this->hasChildren()) {
			if (false === $skipCheck && !$this->needRewindChildrenStack()) {
				throw new Exception("The current children stack needs not to be rewinded untill it reaches end");
			}
			reset($this->childrenActionStack);
			$this->needRewindChildrenStack = false;
			//already place the first child in current child
			$this->goToNextChildInStack();
		}
	}
	
	/**
	 * Allow multiple child actions per action
	 * There may be many child actions per parent
	 * so $this->childrenActionStack can be an array
	 * of AbstractAction or just
	 * a AbstractAction
	 * 
	 * @return unknown_type
	 */
	public function setChild(AbstractAction $action)
	{
		//ad child to stack
		$this->childrenActionStack[] = $action;
		
		if (count($this->childrenActionStack) === 1) {
			$this->goToNextChildInStack();
		}

		//also set the parent and root of the $action in parameter
		$action->setParent($this, $this);
		$action->setRoot($this->getRoot(), $this);
	}
	
	/**
	 * 
	 * @param AbstractAction $action
	 * @return unknown_type
	 */
	public function addChild(AbstractAction $action)
	{
		$this->setChild($action);
	}
	
	/**
	 * The group from parent results where this action
	 * will take its input data
	 * 
	 * @param $groupNumber
	 * @return unknown_type
	 */
	public function setInputDataGroup($groupNumber)
	{
		if (!($this->getParent() instanceof Extract)) {
			throw new Exception('The parent action must be of type Extract in order to set the group for input data');
		}
		$this->groupForInputData = (integer) $groupNumber;
	}
	
	/**
	 * Tell whether engine should create new video entity
	 * instance from this action
	 * @TODO the new instance generatig point has a flaw when it is attached to an Extract action with matchAll
	 * we have to hook someplace the new instance generation so that there is an instance for every match in matchall
	 * 1. find at which point matchAll can give a hint on the numer of results.
	 * 2. once we have the number, add some sort of communication between the Extract action, and the Engine::manageNIGP()
	 * 3. make sure that all those instances are saved gracefully. 
	 * 
	 * @return boolean
	 */
	public function isNewInstanceGeneratingPoint()
	{
		return $this->isNewInstanceGeneratingPoint;
	}
	
	/**
	 * Set the video entity starting point to true
	 * @return unknown_type
	 */
	public function setAsNewInstanceGeneratingPoint()
	{
		$this->isNewInstanceGeneratingPoint = true;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function setAsOptional($bool)
	{
		$this->isOpt = (boolean) $bool;
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function isOptional()
	{
		return $this->isOpt;
	}
	
	/**
	 * Returns the result of the action
	 * 
	 * @param $groupNumber if the result is divided into groups, this specifies which group to return
	 * @return unknown_type
	 */
	abstract public function getResult($groupNumber = null);
	
	/**
	 * Tells the engine whether to call spitt() or not
	 * 
	 * @return unknown_type
	 */
	abstract public function hasFinalResults();

	/**
	 * Once executed, if the action has final results it will
	 * return them as an assciative array
	 * ex : array(VideoName=>'the big lebowsky')
	 * otherwise it will return false
	 * @return unknown_type
	 */
	abstract public function spit();
	
	/**
	 * Will make the results available to the instance
	 * an will return true or false on success or fail
	 * 
	 * @return boolean
	 */
	abstract protected function execute();
	
	/**
	 * 
	 * 
	 * @return unknown_type
	 */
	final public function execute()
	{
		Out::l2("Abstract::execute({$this->getId()})\n");
		if (true === $this->isOpt 
		  && $this->getParent() instanceof Extract 
	      && null !== $this->groupForInputData 
	      && false === ($res = $this->getParent()->hasGroupNumber($this->groupForInputData))) {
			Out::l2("Abstract : execute() : \$this->getParent()->hasGroupNumber({$this->groupForInputData}) = var_dump:\n");
			var_dump($res);
			return self::NO_INPUT_IS_OPT_NO_EXEC;
		}
		
		//call sublcass function
		$res = $this->execute();
		
		//allow exceptions
		if (false === $this->throwOnFalse()) {
			throw new Exception("function AbstractAction/Subclass::thorwOnFalse() returned false.");
		}
		
		return $res;
	}
	
	/**
	 * This allows subclasses and abstract class
	 * to make each other execution crash in case some
	 * condition defined in here is not met.
	 * 
	 * To extend this function you should return
	 * the parent result coupled with your check
	 * result and an && operator
	 * 
	 * @return unknown_type
	 */
	protected function throwOnFalse()
	{
		//make sure lastInput was set by subclass
		//after execution
		return (null !== $this->lastInput);
	}
	
	/**
	 * Clear will empty $results so new info is considered
	 * However if the $results is an array from where different
	 * video entities have to take information, it will only shift
	 * the first result
	 * 
	 * @note make sure to call clear from the last child
	 * 
	 * @return bolean if true the downwards process can continue otherwise call clear() on parent action
	 * i.e. if true, there are more results and the child action should be executed again
	 */
	public function clear()
	{
		Out::l2("Abstract::clear({$this->getId()})\n");
		if (false === $this->isExecuted) {
			throw new Exception('You cannot clear the instance it has not been executed already call execute().');
		}

		//some children need to execute on the same result
		Out::l2("Abstract::clear() : has more children to execute? : ");
		if ($this->goToNextChildInStack()) {
			Out::l2("yes!\n"); 
			return self::MORE_CHILDREN_TO_EXECUTE;
		}
		Out::l2("no! \n");
		//all children were executed for this result
		//renew the children if it is an array
		$this->rewindChildrenStack();
		
		//execute subclass clear
		return $this->clear();
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	abstract protected function clear();
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function fullClear()
	{
		Out::l2("Abstract::fullClear({$this->getId()})\n");
		$this->rewindChildrenStack(true);
		$this->fullClear();
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	abstract protected function fullClear();
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function clearMeAndOffspring($thisFull = true)
	{
		Out::l3("MyId : {$this->getId()}\n");
		if ($this->hasChildren()) {
			foreach ($this->getChildren() as $child) {
				Out::l3("ParentId : {$this->getId()} => ");
				$child->clearMeAndOffspring();
			}
		}
		if (true === $thisFull) {
			$this->fullClear();
		} else {
			return $this->clear();
		}
	}
}