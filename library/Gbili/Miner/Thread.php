<?php
namespace Gbili\Miner;

use Gbili\Miner\BluePrint\Action\AbstractAction;

/**
 * This class will hold the current
 * action being executed and the
 * code returned by the function 
 * that is executing it.
 * 
 * @author gui
 *
 */
class Thread
{
	/**
	 * The action being executed
	 * 
	 * @var Miner_Engine_BluePrint_Action_Abstract
	 */
	private $action = null;
	
	/**
	 * Contains execution status code
	 * (ACTION_EXECUTION_SUCCEED|FAIL etc.)
	 * 
	 * @var integer
	 */
	private $code = null;
	
	/**
	 * 
	 * @param Miner_Engine_BluePrint_Action_Abstract $action
	 * @param unknown_type $code
	 * @return unknown_type
	 */
	public function __construct(AbstractAction $action = null, $statusCode = null)
	{
		if (null !== $action) {
			$this->action = $action;
		}

		if (null !== $statusCode) {
			$this->code = $statusCode;
		}
	}
	
	/**
	 * 
	 * @return Miner_Engine_BluePrint_Action_Abstract
	 */
	public function getAction()
	{
		if (null === $this->action) {
			throw new Exception("The action is not set");
		}
		return $this->action;
	}
	
	/**
	 * 
	 * @param Miner_Engine_BluePrint_Action_Abstract $action
	 * @return void
	 */
	public function setAction(AbstractAction $action)
	{
		$this->action = $action;
	}
	
	/**
	 * 
	 * @return integer
	 */
	public function getStatus()
	{
		if (null === $this->code) {
			throw new Exception("The code is not set");
		}
		return $this->code;
	}
	
	/**
	 * 
	 * @param integer $code
	 * @return void
	 */
	public  function setStatus($code)
	{
		$this->code = $code;
	}
}