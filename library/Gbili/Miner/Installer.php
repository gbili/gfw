<?php
namespace Gbili\Miner;

use Gbili\Db\Req\Admin;

class Installer
{
    const TABLES_SQL_FILE_RELATIVE_PATH = '/config/tables.sql';
    
    /**
     * What Gbili\Db\Req\AbstractReq prefixed adapter
     * is to be used?
     * defaults to __NAMESPACE__, which may not be registered...
     * @var string
     */
    private $adminReqAdapterPrefix = null;
    
    /**
     * Table names all match this regex
     * @var unknown_type
     */
    private $allTablesMatchRegex = '#(BPAction)|(BluePrint)#';
    
    /**
     * On install fail contains message
     * @var string
     */
    private $error = '';
    
    /**
     * 
     * @var \Gbili\Db\Req\Admin
     */
    private $adminReq = null;
    
    /**
     * 
     * @param unknown_type $adminReqAdapterPrefix
     */
    public function __construct($adminReqAdapterPrefix = null)
    {
        if (null === $adminReqAdapterPrefix) {
            $adminReqAdapterPrefix = __NAMESPACE__;
        }
    }
    
    /**
     * Create tables from config/tables.sql
     * @param unknown_type $deleteExisting
     * @throws Exception
     * @return boolean
     */
    public function install($deleteExisting=false)
    {
        $schemaFilePath = __DIR__ . self::TABLES_SQL_FILE_RELATIVE_PATH;
        if (!file_exists($schemaFilePath)) {
            $this->error = 'Check out your miner library, '. $schemaFilePath .'is missing';
            return false;
        }
        
        $sql = file_get_contents($schemaFilePath);
        if (false === $sql) {
            $this->error = 'Could not get contents';
            return false;
        }
        
        if ($deleteExisting === false && ($minerTables = $this->getAdminReq()->getTablesFromRegex($this->allTablesMatchRegex)) && !empty($minerTables)) {
            $this->error = 'The database already has tables, call install(true) to overwrite them';
            return false;
        }
        
        $this->uninstall();
        $this->getAdminReq()->insertUpdateData($sql);
        return true;
    }
    
    /**
     * Delete miner related database tables
     * So if none are related, no tables are deleted
     */
    public function uninstall()
    {
        $tablesToDelete = $this->getAdminReq()->getTablesFromRegex($this->allTablesMatchRegex);
        $this->getAdminReq()->deleteTables($tablesToDelete);
    }
    
    /**
     * 
     * @return \Gbili\Db\Req\Admin
     */
    public function getAdminReq()
    {
        if (null === $this->adminReq) {
            $this->adminReq = new Admin($this->adminReqAdapterPrefix);
        }
        return $this->adminReq;
    }
    
    /**
     * 
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}