<?php
namespace Gbili\Time\Ago;

use Gbili\Regex\AbstractRegex;

class Regex
extends AbstractRegex
{
	/**
	 * 
	 * @param unknown_type $input
	 * @param Url_Regex_String $regexStringObject
	 * @return unknown_type
	 */
	public function __construct($input, Regex\String $regexStringObject)
	{
		parent::__construct($input, $regexStringObject);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getNumber()
	{
		return $this->getMatches(1);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasYears()
	{
		return $this->hasGroupNumber(2);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasMonths()
	{
		return $this->hasGroupNumber(3);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasDays()
	{
		return $this->hasGroupNumber(4);
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function hasHours()
	{
		return $this->hasGroupNumber(5);
	}
}